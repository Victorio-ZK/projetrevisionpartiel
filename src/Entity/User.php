<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: '`user`')]
#[UniqueEntity(fields: ['email'], message: 'There is already an account with this email')]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, unique: true)]
    private ?string $username = null;

    #[ORM\Column(length: 255, unique: true)]
    private ?string $email = null;

    #[ORM\Column(length: 255)]
    private ?string $password = null;

    #[ORM\Column]
    private array $roles = [];

    #[ORM\Column(type: 'boolean')]
    private $isVerified = false;





    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): static
    {
        $this->username = $username;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): static
    {
        $this->password = $password;

        return $this;
    }



    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): static
    {
        $this->roles = $roles;

        return $this;
    }

    //Check si user === admin
    public function isAdmin(){
        return in_array('ROLE_ADMIN', $this->getRoles());
    }


//    public function serialize(): string
//    {
//        return serialize([
//            $this->id,
//            $this->email,
//            $this->roles,
//            $this->password,
//            // Ajoutez d'autres propriétés à sérialiser ici si nécessaire
//        ]);
//    }
//
//    public function unserialize($serialized): void
//    {
//        [
//            $this->id,
//            $this->email,
//            $this->roles,
//            $this->password,
//            // Dé-sérialisez d'autres propriétés ici si nécessaire
//        ] = unserialize($serialized, ['allowed_classes' => false]);
//    }
//
//    public function __serialize(): array
//    {
//        return [
//            $this->id,
//            $this->email,
//            $this->roles,
//            $this->password,
//            // Ajoutez d'autres propriétés à sérialiser ici si nécessaire
//        ];
//    }
//
//    public function __unserialize(array $data): void
//    {
//        [
//            $this->id,
//            $this->email,
//            $this->roles,
//            $this->password,
//            // Dé-sérialisez d'autres propriétés ici si nécessaire
//        ] = $data;
//    }

    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): static
    {
        $this->isVerified = $isVerified;

        return $this;
    }
}
