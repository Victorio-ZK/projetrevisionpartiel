<?php

namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

#[Route(name: 'app_')]
class SecurityController extends AbstractController
{
    #[Route('/login', name: 'login')]
    public function index(Request $request, AuthenticationUtils $utils): Response
    {
        $error = $utils->getLastAuthenticationError(); // récupérer lers erreurs
        $lastUsername = $utils->getLastUsername(); // Permet de laisser les champs en mémoire

        return $this->render('Security/login.html.twig', [
            'error' => $error,
            'last_username' => $lastUsername
        ]);

    }

    #[Route('/logout', name: 'logout')]
    public function logout(){ }
}