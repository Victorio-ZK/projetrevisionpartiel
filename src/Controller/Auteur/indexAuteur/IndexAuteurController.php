<?php

namespace App\Controller\Auteur\indexAuteur;

use AllowDynamicProperties;
use App\Entity\Auteur;
use App\Form\AuteurType;
use App\Repository\AuteurRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[AllowDynamicProperties] #[Route(name: 'app_')]
#[IsGranted('ROLE_USER')]
final class IndexAuteurController extends AbstractController
{

    protected $slugger;
    protected $emInterface;
    protected $logger;
    protected $auteurRepository;

    public function __construct(AuteurRepository $auteurRepository, LoggerInterface $logger, SluggerInterface $slugger,EntityManagerInterface $emInterface)
    {
        $this->logger = $logger;
        $this->slugger = $slugger;
        $this->emInterface = $emInterface;
        $this->AuteurRepository = $auteurRepository;
    }

    #[Route('/auteur', name: 'index_auteur')]
    public function index(Request $request): Response
    {
        $Auteur = new Auteur();
        $form = $this->createForm(AuteurType::class, $Auteur);
        $form->handleRequest($request);

        $Auteurs = $this->AuteurRepository->findAll();


        if ($form->isSubmitted() && $form->isValid()) {
            $image = $form->get('image')->getData();

            if ($image) {
                $originalFilename = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = $this->slugger->slug($originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$image->guessExtension();
                $image->move(
                    $this->getParameter('Auteur_directory'),
                    $newFilename
                );
                $Auteur->setImage($newFilename);
            }
            // persist() : prépare la requête.
            $this->emInterface->persist($Auteur);
            // flush() : exécute la requête.
            $this->emInterface->flush();
            //$this->addFlash('success', 'l\'article nommé '.$Article->getNomArticle().' ajouté avec succès !');
            return $this->redirectToRoute('app_index_auteur');
        }


        return $this->render('AuteurScreen/index.html.twig', [
            'paramsDefault' => [],
            'formAuteur' => $form->createView(),
            'auteurs' => $Auteurs
        ]);
    }
}
