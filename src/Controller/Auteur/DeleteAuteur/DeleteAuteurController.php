<?php

namespace App\Controller\Auteur\DeleteAuteur;

use AllowDynamicProperties;
use App\Entity\Auteur;
use App\Form\AuteurType;
use App\Repository\AuteurRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[AllowDynamicProperties] #[Route(name: 'app_')]
#[IsGranted('ROLE_USER')]
final class DeleteAuteurController extends AbstractController
{
    protected $slugger;
    protected $emInterface;
    protected $logger;
    protected Auteur $auteurRepository;

    public function __construct(AuteurRepository $auteurRepository, LoggerInterface $logger, SluggerInterface $slugger,EntityManagerInterface $emInterface)
    {
        $this->logger = $logger;
        $this->slugger = $slugger;
        $this->emInterface = $emInterface;
        $this->AuteurRepository = $auteurRepository;
    }


    #[Route('/auteur/delete/{id}', name: 'delete_auteur')]
    public function deleteAuteur($id): RedirectResponse
    {
        $auteur = $this->emInterface->getRepository(Auteur::class)->find($id);
        $this->emInterface->remove($auteur);
        $this->emInterface->flush();

        return $this->redirectToRoute('app_index_auteur');
    }
}
