<?php

namespace App\Controller\Auteur\updateAuteur;


use AllowDynamicProperties;
use App\Entity\Auteur;
use App\Form\AuteurType;
use App\Repository\AuteurRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\String\Slugger\SluggerInterface;

#[AllowDynamicProperties] #[Route(name: 'app_')]
#[IsGranted('ROLE_USER')]
final class UpdateAuteurController extends AbstractController
{

    protected $slugger;
    protected $emInterface;
    protected $logger;
    protected $auteurRepository;

    public function __construct(AuteurRepository $auteurRepository, LoggerInterface $logger, SluggerInterface $slugger,EntityManagerInterface $emInterface)
    {
        $this->logger = $logger;
        $this->slugger = $slugger;
        $this->emInterface = $emInterface;
        $this->AuteurRepository = $auteurRepository;
    }

    #[Route('/Auteur/edit/{id}', name: 'edit_auteur')]
    public function __invoke(Request $request, Auteur $auteur)
    {
        $form = $this->createForm(AuteurType::class, $auteur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) { //pas de flush nécéssaire
            $this->emInterface->flush();
            //$this->addFlash('success', 'Tome numéro '.$auteur->getTomeNumber().' Modifié avec succès !');
            return $this->redirectToRoute('app_index_auteur');
//          return dd('meeerde');
        }

        return $this->render('AuteurScreen/edit.html.twig', [
            'formAuteurEdit' => $form->createView(),
            'auteur' => $auteur
        ]);
    }
}
