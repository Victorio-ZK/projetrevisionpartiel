<?php

namespace App\Controller\Livre\DeleteAuteur;


use App\Entity\Livre;
use AllowDynamicProperties;
use App\Repository\LivreRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\String\Slugger\SluggerInterface;


#[AllowDynamicProperties]
#[Route(name: 'app_')]
#[IsGranted('ROLE_USER')]
class DeleteLivreController extends AbstractController
{

    protected $slugger;
    protected $emInterface;
    protected $logger;
    protected $livreRepository;

    public function __construct(LivreRepository $livreRepository, LoggerInterface $logger, SluggerInterface $slugger,EntityManagerInterface $emInterface)
    {
        $this->logger = $logger;
        $this->slugger = $slugger;
        $this->emInterface = $emInterface;
        $this->livreRepository = $livreRepository;
    }


    #[Route('/livre/delete/{id}', name: 'delete_livre')]
    public function __invoke($id): RedirectResponse
    {
        $livre = $this->emInterface->getRepository(Livre::class)->find($id);
//      $this->emInterface->remove($livre);

        $livre->setDeleted(true);
        $this->emInterface->flush();

        $this->addFlash('success', 'Livre Archivée avec succès');
        return $this->redirectToRoute('app_index_livre');
    }
}
