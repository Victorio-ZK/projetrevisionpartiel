<?php

namespace App\Controller\Livre\indexLivre;

use App\Entity\Livre;
use App\Form\LivreType;
use App\Repository\LivreRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\String\Slugger\SluggerInterface;

#[Route(name: 'app_')]
#[IsGranted('ROLE_USER')]
class IndexLivreController extends AbstractController
{
    protected $slugger;
    protected $emInterface;
    protected $logger;
    protected $LivreRepository;

    public function __construct( LivreRepository $LivreRepository, LoggerInterface $logger, SluggerInterface $slugger,EntityManagerInterface $emInterface)
    {
        $this->logger = $logger;
        $this->slugger = $slugger;
        $this->emInterface = $emInterface;
        $this->LivreRepository = $LivreRepository;
    }

    #[Route('/livre', name: 'index_livre')]
    public function index(Request $request): Response
    {
        $Livre = new Livre();
        $form = $this->createForm(LivreType::class, $Livre);
        $form->handleRequest($request); // Check ce qui se passe au niveau du form.

        $Livres = $this->LivreRepository->getlist();
        //$Livres = $this->LivreRepository->findAll();

        if ($form->isSubmitted() && $form->isValid()) {
            $image = $form->get('image')->getData();

            if ($image) {
                $originalFilename = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = $this->slugger->slug($originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$image->guessExtension();
                $image->move(
                    $this->getParameter('Livre_directory'),
                    $newFilename
                );
                $Livre->setImage($newFilename);
            }
            // persist() : prépare la requête.
            $this->emInterface->persist($Livre);
            // flush() : exécute la requête.
            $this->emInterface->flush();

            //$this->addFlash('success', 'l\'article nommé '.$Article->getNomArticle().' ajouté avec succès !');
            return $this->redirectToRoute('app_index_livre');
        }

        return $this->render('LivreScreen/index.html.twig', [
            'paramsDefault' => [],
            'formLivre' => $form->createView(),
            'livres' =>$Livres,
        ]);
    }





}
