<?php

namespace App\Controller\Livre\updateLivre;

use App\Entity\Livre;
use App\Form\AuteurType;
use App\Form\LivreType;
use App\Repository\LivreRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\String\Slugger\SluggerInterface;

#[Route(name: 'app_')]
#[IsGranted('ROLE_USER')]
final class UpdateLivreController extends AbstractController
{
    protected $slugger;
    protected $emInterface;
    protected $logger;
    protected $LivreRepository;

    public function __construct( LivreRepository $LivreRepository, LoggerInterface $logger, SluggerInterface $slugger,EntityManagerInterface $emInterface)
    {
        $this->logger = $logger;
        $this->slugger = $slugger;
        $this->emInterface = $emInterface;
        $this->LivreRepository = $LivreRepository;
    }



    #[Route('/livre/edit/{id}', name: 'edit_livre')]
    public function __invoke(Request $request, Livre $livre)
    {
        $form = $this->createForm(LivreType::class, $livre);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) { //pas de flush nécéssaire
            $this->emInterface->flush();
            //$this->addFlash('success', 'Tome numéro '.$auteur->getTomeNumber().' Modifié avec succès !');
            return $this->redirectToRoute('app_index_livre');
//          return dd('meeerde');
        }
        return $this->render('LivreScreen/edit.html.twig', [
            'formLivreEdit' => $form->createView(),
            'livre' => $livre,
            'canCreateBook' => $this->isGranted('ROLE_USER')
        ]);
    }
}
