<?php
namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapQueryParameter;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\String\Slugger\SluggerInterface;

#[Route(name: 'app_')]
#[IsGranted('ROLE_USER')]
final class MainController extends AbstractController //Final=> "Peut pas faire d'héritage de cette classe"
{
    protected $slugger;
    protected $emInterface;
    protected $logger;

    public function __construct(LoggerInterface $logger, SluggerInterface $slugger,EntityManagerInterface $emInterface)
    {
        $this->logger = $logger;
        $this->slugger = $slugger;
        $this->emInterface = $emInterface;
    }
    #[\Symfony\Component\Routing\Annotation\Route('/', name: 'index', methods: 'GET')]
    public function __invoke(Request $request,
                             #[MapQueryParameter(name: 'ville')]
                             ?string $ville = null,)
    {
        $this->logger->info("j'ai recu le params {$ville}");
//      dump($request->query->get('name'));
        $tableau = [1,2,3,4,5,6,7,8,9,10];
        $salut = $request->query->get('name') ?? 'default name';
        //return new Response("hello World");
        return $this->render('index/index.html.twig', [
            'salut' => $salut,
            'tableau' => $tableau,
            'ville' => $ville
        ]);
    }
}
