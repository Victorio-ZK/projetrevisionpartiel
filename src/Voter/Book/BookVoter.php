<?php

namespace App\Voter\Book;

use App\Entity\Livre;
use App\Enum\Role\RoleEnum;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

final class BookVoter extends Voter
{
 protected function supports(string $attribute, mixed $subject): bool
 {
     return $attribute === 'can_delete_book';
 }

 protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
 {
     if (
         in_array(
             needle: RoleEnum::ROLE_ADMIN->value,
             haystack: $token->getUser()->getRoles()
         )
     ){
         return $subject instanceof Livre
             && $subject->isDeleted() === false;
         }
     return true;
 }
}