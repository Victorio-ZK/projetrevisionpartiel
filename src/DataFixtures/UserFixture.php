<?php

namespace App\DataFixtures;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixture extends Fixture
{
    private $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    public function load(ObjectManager $manager): void
    {
        $user = new User();

        $user->setUsername('root');
        $user->setEmail('admin@gmail.com');
        $user->setIsVerified(false);
        $user->setRoles([]);
        $user->setPassword($this->passwordHasher->hashPassword(
            $user,
            'root'
        ));

        $manager->persist($user);
        $manager->flush();
    }
}
