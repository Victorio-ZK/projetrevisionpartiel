<?php

namespace App\DataFixtures;

use App\Entity\Auteur;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class AuteurFixtures extends Fixture
{



    public const AUTEUR_REFERENCE = 'Skyrim-User';
    public function load(ObjectManager $manager): void
    {
        $fakerFactory = Factory::create('fr_FR');


            $auteur = new Auteur();
            $auteur->setName($fakerFactory->firstName);
            $auteur->setLastname($fakerFactory->lastName);
            $auteur->setImage('Rayya-663822e3ee94f.png');
            $auteur->setAge($fakerFactory->numberBetween($min = 10, $max = 80));
            $this->addReference(self::AUTEUR_REFERENCE, $auteur);
            $manager->persist($auteur);

        $manager->flush();
    }
}