<?php

namespace App\DataFixtures;

use App\Entity\Livre;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class LivreFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $fakerFactory = Factory::create('fr_FR');

        for ($count = 0; $count < 5; $count++) {
            $livre = new Livre();
            $livre->setName('Volume'.$count);
            $livre->setDescription($fakerFactory->realText());
            $livre->setImage('Book01-663826a85d334.webp');
            $livre->setAuteur($this->getReference(AuteurFixtures::AUTEUR_REFERENCE));
            $manager->persist($livre);
        }
        $manager->flush();
    }


    public function getDependencies(): array
    {
        return [
            AuteurFixtures::class
        ];
    }
}