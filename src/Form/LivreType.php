<?php

namespace App\Form;

use App\Entity\Auteur;
use App\Entity\Livre;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Regex;

class LivreType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name',   TextType::class,
                options: [
                    'constraints' => [
                        new NotBlank(),
                        new Length(min:1, max:200),
                        new NotNull(),
                    ],
                    'attr' => ['class' => 'form-control', 'placeholder' => 'app.forms.contact.name.label']
                ]
            )
            ->add('description',   TextType::class,
                options: [
                    'constraints' => [
                        new NotBlank(),
                        new Length(min:1, max:1000),
                        new NotNull(),
                    ],
                    'attr' => ['class' => 'form-control', 'placeholder' => 'app.forms.contact.name.label']
                ]
            )

            ->add('image',   FileType::class,
                options: [
                    //https://stackoverflow.com/questions/40983353/the-forms-view-data-is-expected-to-be-an-instance-of-class-but-is-an-stri
                    'mapped' => false,
                    'required' => false,
                    //https://stackoverflow.com/questions/40983353/the-forms-view-data-is-expected-to-be-an-instance-of-class-but-is-an-stri
                    'constraints' => [
                        new NotBlank(),
                        new Length(min:1, max:200),
                        new NotNull(),
                    ],
                    'attr' => ['class' => 'form-control', 'placeholder' => 'app.forms.contact.name.label']
                ]
            )
            ->add('Auteur', EntityType::class, [
                'class' => Auteur::class,
                'choice_label' => 'id',
                'constraints' => [
                    new NotBlank(),
                    new NotNull(),
                ],
                'attr' => ['class' => 'form-control']
            ])
            ->add('submit',  SubmitType::class, ['attr' => ['class' => 'form-control', 'placeholder' => 'app.forms.contact.submit.label']])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Livre::class,
        ]);
    }
}
